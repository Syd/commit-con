mod interactive;

use std::{fmt::Display, fs::File, io::Read, path::Path};

use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Config {
    #[serde(rename = "type")]
    types: Select,
    scope: Option<Select>,
    subject: Subject,
    body: Body,
    #[serde(rename = "footer")]
    footers: Vec<Footer>,
}

#[derive(Debug, Deserialize)]
pub struct Select {
    desc: String,
    #[serde(rename = "value")]
    values: Vec<Item>,
}

#[derive(Debug, Deserialize)]
pub struct Item {
    name: String,
    desc: String,
}

#[derive(Debug, Deserialize)]
pub struct Subject {
    desc: String,
    #[serde(rename = "template")]
    templates: Vec<Template>,
}

#[derive(Debug, Deserialize)]
pub struct Template {
    name: String,
    desc: String,
    template: String,
}

#[derive(Debug, Deserialize)]
pub struct Body {
    desc: String,
}

#[derive(Debug, Deserialize)]
pub struct Footer {
    name: String,
    desc: String,
    query: Option<String>,
}

#[derive(Debug)]
pub enum Error {
    File(std::io::Error),
    Deserialize(toml::de::Error),
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Self::File(e)
    }
}

impl From<toml::de::Error> for Error {
    fn from(e: toml::de::Error) -> Self {
        Self::Deserialize(e)
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self {
            Error::File(file_err) => write!(f, "Invalid input file: {file_err}"),
            Error::Deserialize(deserialize_err) => {
                write!(f, "Occur when deserialize toml: {deserialize_err}")
            }
        }
    }
}

impl std::error::Error for Error {}

impl Config {
    pub fn from_path(path: &Path) -> Result<Self, Error> {
        let mut file = File::options().read(true).open(&path)?;
        let mut str = String::new();
        file.read_to_string(&mut str)?;
        Ok(toml::from_str(&str)?)
    }
}

#[test]
fn read_test() {
    let path = Path::new("commit.toml");
    let config = Config::from_path(&path);
    dbg!(&config);
    assert!(config.is_ok());
}
