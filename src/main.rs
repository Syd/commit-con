#![feature(iterator_try_collect)]
mod config;
mod env_args;

pub use config::Config;
use env_args::AppArgs;

use clap::Parser;

fn main() -> std::io::Result<()> {
    let args = AppArgs::parse();

    let config = Config::from_path(args.get_path()).unwrap();

    let commit_text = config.interacte()?;
    dbg!(&commit_text);
    println!("{:#?}", commit_text.lines().collect::<Vec<&str>>());
    Ok(())
}
