use clap::Parser;

#[derive(Debug, Parser)]
pub struct AppArgs {
    #[clap(parse(from_os_str), short = 'p', long = "path")]
    path: std::path::PathBuf,
}

impl AppArgs {
    pub fn get_path(&self) -> &std::path::Path {
        &self.path
    }
}
