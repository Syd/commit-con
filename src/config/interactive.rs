use dialoguer::{theme::ColorfulTheme, Confirm, FuzzySelect, Input};

use crate::Config;

use super::{Body, Footer, Item, Select, Subject, Template};

impl ToString for Item {
    fn to_string(&self) -> String {
        format!("{}: {:>5}", self.name, self.desc)
    }
}

impl Select {
    pub fn select(&self) -> std::io::Result<&String> {
        FuzzySelect::with_theme(&ColorfulTheme::default())
            .with_prompt(&self.desc)
            .items(&self.values)
            .default(0)
            .interact()
            .map(|index| &self.values[index].name)
    }
}

impl ToString for Template {
    fn to_string(&self) -> String {
        format!("{}: {:>5}", self.name, self.desc)
    }
}

impl Subject {
    pub fn select(&self) -> std::io::Result<String> {
        FuzzySelect::with_theme(&ColorfulTheme::default())
            .with_prompt("Select a template? (press ECS to skip)")
            .items(&self.templates)
            .default(0)
            .interact_opt()
            .and_then(|index| {
                if let Some(index) = index {
                    let template = &self.templates[index].template;
                    Input::with_theme(&ColorfulTheme::default())
                        .with_prompt(format!("{}\n", &self.desc))
                        .with_initial_text(template)
                        .interact_text()
                } else {
                    Input::with_theme(&ColorfulTheme::default())
                        .with_prompt(format!("{}\n", &self.desc))
                        .interact_text()
                }
            })
    }
}

impl Body {
    pub fn input(&self) -> std::io::Result<String> {
        Input::with_theme(&ColorfulTheme::default())
            .with_prompt(format!("{} (press Enter to skip)\n", &self.desc))
            .allow_empty(true)
            .interact_text()
    }
}

impl Footer {
    pub fn input(&self) -> std::io::Result<String> {
        if let Some(query) = &self.query {
            if Confirm::with_theme(&ColorfulTheme::default())
                .with_prompt(query)
                .interact()?
            {
                Input::with_theme(&ColorfulTheme::default())
                    .with_prompt(format!("{}\n", &self.desc))
                    .interact_text()
            } else {
                Ok(String::new())
            }
        } else {
            Input::with_theme(&ColorfulTheme::default())
                .with_prompt(&self.desc)
                .interact_text()
        }
    }
}

impl Config {
    pub fn interacte(&self) -> std::io::Result<String> {
        let types = self.types.select()?;
        let scope = if let Some(scope) = &self.scope {
            let scope = scope.select()?;
            format!("({scope}):")
        } else {
            String::new()
        };
        let subject = self.subject.select()?;
        let body = self.body.input()?;
        let body = if !body.is_empty() {
            format!("\n\n{body}\n\n")
        } else {
            body
        };
        let footers = self
            .footers
            .iter()
            .map(|footer| footer.input())
            .try_collect::<Vec<String>>()
            .map(|footers| {
                footers
                    .into_iter()
                    .filter(|str| !str.is_empty())
                    .collect::<Vec<String>>()
                    .join("\n\n")
            })?;

        Ok(format!("{types}{scope}{subject}{body}{footers}"))
    }
}
